import * as React from 'react';
import  { Component } from 'react'
import * as d3 from 'd3'
import { VegaLite } from 'react-vega'
import blogs from '../server/db/blogs';
import barDataplot from './blogs'


const spec = {
	width: 400,
	height: 200,
	mark: 'bar',
	encoding: {
	  x: { field: 'productName', type: 'ordinal' },
	  y: { field: 'buyPrice', type: 'quantitative' },
	},
	data: { name: 'table' }, 
  }
   

 

  class BarChart extends Component {
	 
	  render() { return <div ref="canvas">
		  <VegaLite spec={spec} data={barDataplot} />
	  </div> }
  }

class App extends React.Component<IAppProps, IAppState> {
	constructor(props: IAppProps) {
		super(props);
		this.state = {
			blogs: []
		};
		
	}

	async componentDidMount() {
		try {
			let r = await fetch('/api/blogs');
			let blogs = await r.json();
			this.setState({ blogs });
		} catch (error) {
			console.log(error);
		}
	}

	render() {
		return (
			<main className="container mt-9">
				<h1 className="text-secondary text-center bg-info mb-5">Data Pulling MySql API</h1>

				<div className="container">
    <div className="row">
        <div className="col-xs-6 pre-scrollable ml-5" style={{height: "100vh"}}>
		<ul className="list-group">
		<table className="table table-dark">
  <thead>
    <tr>
      <th scope="col">Model Name</th>
      <th scope="col">Price</th>
    </tr>
  </thead>
			
	{this.state.blogs.map(blogs => {
		   return <tbody> <tr> <td>{blogs.productName}</td> <td>{blogs.buyPrice}</td> </tr> </tbody> 
	   
				   })}
				   </table>
</ul>
        </div>
        <div className="col-xs-6">
		<BarChart/>	
        </div>
    </div>
</div>

				




	</main>
		);
	}
}

export interface IAppProps {}

export interface IAppState {
	blogs: Array<{productName : string , buyPrice : number }>;
}

export default App;